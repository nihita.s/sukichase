# Suki Chase

This game was developed using Godot's Make Your First 2D Game Tutorial as a base and adding features to it.

Running Instructions
- Clone the game repository to your machine
- Open the Godot project manager/console and click import
- Navigate to the directory the game repository was downloaded in and select the repository's project.godot file in the folder Assignment 0
- The project will now appear in the project manager and can be selected
- To build and run the project, hit the run button on the upper right hand corner of the screen

In this game, I was inspired by my dog Suki and chasing her around the house after she steals food off of our counters. I drew the player and food sprites by hand and the mob sprites were provided by the tutorial I used as a base.

Demo Link: https://youtu.be/2JSjA10VOOg