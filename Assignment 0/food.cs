using Godot;
using System;

public partial class food : Area2D{
	// Called when the node enters the scene tree for the first time.
	public override void _Ready(){
		GD.Print("food ready");
		Hide();
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta){
		var animatedSprite2D = GetNode<AnimatedSprite2D>("AnimatedSprite2D");
		animatedSprite2D.Play();
		animatedSprite2D.Animation = "default";
	}
	
	private void _on_area_entered(Area2D body){
		if (body.Name == "Player"){
			Hide(); //disappear from screen
			QueueFree(); //delete object so player can't collide with it again
		}
	}
	
	public void Start (){
		Show();
		//GetNode<CollisionShape2D>("CollisionShape2D").Disabled = false;
	}
}





