using Godot;

public partial class main : Node{
	[Export]
	public PackedScene MobScene{get;set;}
	[Export]
	public PackedScene FoodScene{get;set;}
	
	private int _score;
	
	private void game_over(){
		GetNode<Timer>("MobTimer").Stop();
		GetNode<hud>("HUD").ShowGameOver();
	}
	
	public void new_game(){
		_score = 0;
		
		var player = GetNode<player>("Player");
		var startPosition = GetNode<Marker2D>("StartPosition");
		player.Start(startPosition.Position);
		
		GetNode<Timer>("StartTimer").Start();
		
		var hud = GetNode<hud>("HUD");
		hud.UpdateScore(_score);
		hud.ShowMessage("Get Ready!");
		
		GetTree().CallGroup("mobs", Node.MethodName.QueueFree);
		GetTree().CallGroup("food", Node.MethodName.QueueFree);
	}
	
	private void _on_mob_timer_timeout(){
		mob mob_obj = MobScene.Instantiate<mob>(); //new instance of mob scene
		
		var mobSpawnLocation = GetNode<PathFollow2D>("MobPath/MobSpawnLocation");
		mobSpawnLocation.ProgressRatio = GD.Randf(); //choose random Path2D location
		
		float direction = mobSpawnLocation.Rotation + Mathf.Pi/2; //set mob dir perpendicular to path dir
		
		mob_obj.Position = mobSpawnLocation.Position; //set mob pos to rand location
		
		direction += (float)GD.RandRange(-Mathf.Pi/4, Mathf.Pi/4);
		mob_obj.Rotation = direction; //add randomness to dir
		
		var velocity = new Vector2((float)GD.RandRange(150.0,250.0),0);
		mob_obj.LinearVelocity = velocity.Rotated(direction); //choose velocity
		
		AddChild(mob_obj); //spawn mob (add to main scene)
	}
	
	public void _on_collect(){
		GD.Print("collect");
		_score++;
		GetNode<hud>("HUD").UpdateScore(_score); //update score on HUD
		
		food food_obj = FoodScene.Instantiate<food>();
		
		var foodSpawnLocation = GetNode<PathFollow2D>("FoodPath/FoodSpawnLocation");
		foodSpawnLocation.ProgressRatio = GD.Randf();
		food_obj.Position = foodSpawnLocation.Position;
		
		AddChild(food_obj);
		food_obj.Show();
	}


	private void _on_start_timer_timeout(){
		GetNode<Timer>("MobTimer").Start();
		food food_obj = FoodScene.Instantiate<food>();
		
		var foodSpawnLocation = GetNode<PathFollow2D>("FoodPath/FoodSpawnLocation");
		foodSpawnLocation.ProgressRatio = GD.Randf();
		food_obj.Position = foodSpawnLocation.Position;
		
		AddChild(food_obj);
		food_obj.Show();
	}
	
	public override void _Ready(){
		
	}
}



